import 'package:get_it/get_it.dart';

import 'domain/auth/auth_service.dart';
import 'domain/dialog_service.dart';
import 'domain/navigation_service..dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => DialogService());
  locator.registerLazySingleton(() => AuthenticationService());
}
