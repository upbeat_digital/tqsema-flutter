

///for user review model to change the variable
class UserReview {
  String createdAt;
  String userId;
  String id;
  double rating;
  String review;
  String uid;
  String userName;

  UserReview(
      {this.createdAt,
      this.userId,
      this.id,
      this.rating,
      this.review,
      this.uid,
      this.userName});

  factory UserReview.fromJson(Map<String, dynamic> json) => UserReview(
        createdAt: json['created_at'] as String,
        userId: json['user_id'] as String,
        id: json['id'] as String,
        rating: (json['rating'] as num)?.toDouble(),
        review: json['review'] as String,
        uid: json['uid'] as String,
        userName: json['user_name'] as String,
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'created_at': createdAt,
        'user_id': userId,
        'id': id,
        'rating': rating,
        'review': review,
        'uid': uid,
        'user_name': userName,
      };
}
