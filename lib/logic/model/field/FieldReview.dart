class FieldReview {
  String createdAt;
  String fieldId;
  String id;
  double rating;
  String review;
  String uid;
  String userName;

  FieldReview(
      {this.createdAt,
      this.fieldId,
      this.id,
      this.rating,
      this.review,
      this.uid,
      this.userName});

  factory FieldReview.fromJson(Map<String, dynamic> json) => FieldReview(
        createdAt: json['created_at'] as String,
        fieldId: json['field_id'] as String,
        id: json['id'] as String,
        rating: (json['rating'] as num)?.toDouble(),
        review: json['review'] as String,
        uid: json['uid'] as String,
        userName: json['user_name'] as String,
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'created_at': createdAt,
        'field_id': fieldId,
        'id': id,
        'rating': rating,
        'review': review,
        'uid': uid,
        'user_name': userName,
      };
}
