import 'package:tqsema_final/logic/model/field/FieldLocation.dart';

class About {
  String address;
  String description;
  String email;
  FieldLocation location;
  String ownerName;
  String phone;
  String website;

  About(
      {this.address,
      this.description,
      this.email,
      this.location,
      this.ownerName,
      this.phone,
      this.website});

  factory About.fromJson(Map<String, dynamic> json) => About(
        address: json['address'] as String,
        description: json['description'] as String,
        email: json['email'] as String,
        location: json['location'] == null
            ? null
            : FieldLocation.fromJson((json['location'] as Map)?.map(
                (k, e) => MapEntry(k as String, e),
              )),
        ownerName: json['owner_name'] as String,
        phone: json['phone'] as String,
        website: json['website'] as String,
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'address': address,
        'description': description,
        'email': email,
        'location': location?.toJson(),
        'owner_name': ownerName,
        'phone': phone,
        'website': website,
      };
}
