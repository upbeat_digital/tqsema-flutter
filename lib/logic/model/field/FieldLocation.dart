class FieldLocation {
  double latitude;
  double longitude;

  FieldLocation({this.latitude, this.longitude});

  factory FieldLocation.fromJson(Map<String, dynamic> json) => FieldLocation(
        latitude: (json['latitude'] as num)?.toDouble(),
        longitude: (json['longitude'] as num)?.toDouble(),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'latitude': latitude,
        'longitude': longitude,
      };
}
