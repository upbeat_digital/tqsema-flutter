class Rent {
  String duration;
  double price;

  Rent(this.duration, this.price);

  factory Rent.fromJson(Map<String, dynamic> json) => Rent(
        json['duration'] as String,
        (json['price'] as num)?.toDouble(),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'duration': duration,
        'price': price,
      };
}
