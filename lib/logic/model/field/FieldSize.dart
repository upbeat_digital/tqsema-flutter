class FieldSize {
  double height;
  double width;

  FieldSize(this.height, this.width);

  factory FieldSize.fromJson(Map<String, dynamic> json) => FieldSize(
        (json['height'] as num)?.toDouble(),
        (json['width'] as num)?.toDouble(),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'height': height,
        'width': width,
      };
}
