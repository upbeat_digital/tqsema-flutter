import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tqsema_final/logic/model/field/Field.dart';
import 'package:tqsema_final/logic/model/field/Rent.dart';
import 'package:tqsema_final/logic/utils/AppUtility.dart';

class BookFieldData {
  DateTime bookedForDateTime;
  DateTime createdAt;
  Field field;
  String id;
  int paymentStatus;
  Rent rent;
  String txnId;
  String userId;

  BookFieldData(
      {this.bookedForDateTime,
      this.createdAt,
      this.field,
      this.id,
      this.paymentStatus,
      this.rent,
      this.txnId,
      this.userId});

  factory BookFieldData.fromJson(Map<String, dynamic> json) => BookFieldData(
        bookedForDateTime:
            AppUtility.stringToDateTime(json['booked_for_date_time'] as String),
        createdAt: AppUtility.stringToDateTime(json['created_at'] as String),
        field: BookFieldData._documentReferenceToField(
            json['field'] as DocumentReference),
        id: json['id'] as String,
        paymentStatus: json['payment_status'] as int,
        rent: json['rent'] == null
            ? null
            : Rent.fromJson((json['rent'] as Map)?.map(
                (k, e) => MapEntry(k as String, e),
              )),
        txnId: json['txn_id'] as String,
        userId: json['user_id'] as String,
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'booked_for_date_time': AppUtility.dateTimeToString(bookedForDateTime),
        'created_at': AppUtility.dateTimeToString(createdAt),
        'field': BookFieldData._fieldTodocumentReference(field),
        'id': id,
        'payment_status': paymentStatus,
        'rent': rent?.toJson(),
        'txn_id': txnId,
        'user_id': userId,
      };

  static Field _documentReferenceToField(DocumentReference reference) {
    return new Field();
  }

  static DocumentReference _fieldTodocumentReference(Field field) {
    return Firestore.instance.collection('fields').document(field.id);
  }
}
