

import '../Rating.dart';
import 'About.dart';
import 'FieldSize.dart';
import 'Rent.dart';

class Field {
  About about;
  double downPaymentPercentage;
  String id;
  List<String> images;
  String name;
  Rating rating;
  List<Rent> rent;
  FieldSize size;
  String type;

  Field(
      {this.about,
      this.downPaymentPercentage,
      this.id,
      this.images,
      this.name,
      this.rating,
      this.rent,
      this.size,
      this.type}) {
    if (this.rating == null) {
      this.rating = Rating();
    }
  }

  factory Field.fromJson(Map<String, dynamic> json) => Field(
        about: json['about'] == null
            ? null
            : About.fromJson((json['about'] as Map)?.map(
                (k, e) => MapEntry(k as String, e),
              )),
        downPaymentPercentage:
            (json['down_payment_percentage'] as num)?.toDouble(),
        id: json['id'] as String,
        images: (json['images'] as List)?.map((e) => e as String)?.toList(),
        name: json['name'] as String,
        rating: json['rating'] == null
            ? null
            : Rating.fromJson((json['rating'] as Map)?.map(
                (k, e) => MapEntry(k as String, e),
              )),
        rent: (json['rent'] as List)
            ?.map((e) => e == null
                ? null
                : Rent.fromJson((e as Map)?.map(
                    (k, e) => MapEntry(k as String, e),
                  )))
            ?.toList(),
        size: json['size'] == null
            ? null
            : FieldSize.fromJson((json['size'] as Map)?.map(
                (k, e) => MapEntry(k as String, e),
              )),
        type: json['type'] as String,
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'about': about?.toJson(),
        'down_payment_percentage': downPaymentPercentage,
        'id': id,
        'images': images,
        'name': name,
        'rating': rating?.toJson(),
        'rent': rent?.map((e) => e?.toJson())?.toList(),
        'size': size?.toJson(),
        'type': type,
      };
}
