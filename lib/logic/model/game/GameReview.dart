class GameReview {
  String createdAt;
  String gameId;
  String id;
  double rating;
  String review;
  String uid;
  String userName;

  GameReview(
      {this.createdAt,
      this.gameId,
      this.id,
      this.rating,
      this.review,
      this.uid,
      this.userName});

  factory GameReview.fromJson(Map<String, dynamic> json) => GameReview(
        createdAt: json['created_at'] as String,
        gameId: json['game_id'] as String,
        id: json['id'] as String,
        rating: (json['rating'] as num)?.toDouble(),
        review: json['review'] as String,
        uid: json['uid'] as String,
        userName: json['user_name'] as String,
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'created_at': createdAt,
        'game_id': gameId,
        'id': id,
        'rating': rating,
        'review': review,
        'uid': uid,
        'user_name': userName,
      };
}
