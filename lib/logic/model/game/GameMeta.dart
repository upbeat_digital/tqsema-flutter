import 'package:tqsema_final/logic/utils/AppUtility.dart';

class GameMeta {
  String fieldName;
  String fieldAddress;
  DateTime gameDateTime;

  GameMeta({this.fieldName, this.fieldAddress, this.gameDateTime});

  factory GameMeta.fromJson(Map<String, dynamic> json) => GameMeta(
        fieldName: json['field_name'] as String,
        fieldAddress: json['field_address'] as String,
        gameDateTime:
            AppUtility.stringToDateTime(json['game_date_time'] as String),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'field_name': fieldName,
        'field_address': fieldAddress,
        'game_date_time': AppUtility.dateTimeToString(gameDateTime),
      };
}
