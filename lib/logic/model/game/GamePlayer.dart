
import '../User.dart';

class GamePlayer {
  User user;
  String role;
  bool isApproved;

  GamePlayer({this.user, this.role, this.isApproved = false});

  factory GamePlayer.fromJson(Map<String, dynamic> json) => GamePlayer(
        user: json['user'] == null
            ? null
            : User.fromJson((json['user'] as Map)?.map(
                (k, e) => MapEntry(k as String, e),
              )),
        role: json['role'] as String,
        isApproved: json['isApproved'] as bool,
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'user': user?.toJson(),
        'role': role,
        'isApproved': isApproved,
      };
}
