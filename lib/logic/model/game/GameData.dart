import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tqsema_final/logic/model/AgeGroup.dart';
import 'package:tqsema_final/logic/model/Rating.dart';
import 'package:tqsema_final/logic/model/field/BookFieldData.dart';
import 'package:tqsema_final/logic/model/game/GameMeta.dart';
import 'package:tqsema_final/logic/utils/AppUtility.dart';

import 'GamePlayer.dart';

class GameData {
  AgeGroup ageGroup;
  BookFieldData booking;
  int bookingType;
  DateTime createdAt;
  String description;
  DateTime gameDateTime;
  GameMeta gameMeta;
  String id;
  String image;
  bool isPrivate;
  String matchType;
  String name;
  int playerNeeded;
  List<GamePlayer> players;
  double price;
  int status;
  Rating rating;
  String userId;

  GameData(
      {this.ageGroup,
      this.booking,
      this.bookingType,
      this.createdAt,
      this.description,
      this.gameDateTime,
      this.gameMeta,
      this.id,
      this.image,
      this.isPrivate,
      this.matchType,
      this.name,
      this.playerNeeded,
      this.players,
      this.price,
      this.status,
      this.rating,
      this.userId}) {
    if (this.rating == null) {
      this.rating = Rating();
    }
  }

  factory GameData.fromJson(Map<String, dynamic> json) => GameData(
        ageGroup: json['age_group'] == null
            ? null
            : AgeGroup.fromJson((json['age_group'] as Map)?.map(
                (k, e) => MapEntry(k as String, e),
              )),
        booking: GameData._documentReferenceToBookFieldData(
            json['booking'] as DocumentReference),
        bookingType: json['booking_type'] as int,
        createdAt: AppUtility.stringToDateTime(json['created_at'] as String),
        description: json['description'] as String,
        gameDateTime:
            AppUtility.stringToDateTime(json['game_date_time'] as String),
        gameMeta: json['game_meta'] == null
            ? null
            : GameMeta.fromJson((json['game_meta'] as Map)?.map(
                (k, e) => MapEntry(k as String, e),
              )),
        id: json['id'] as String,
        image: json['image'] as String,
        isPrivate: json['is_private'] as bool,
        matchType: json['match_type'] as String,
        name: json['name'] as String,
        playerNeeded: json['player_needed'] as int,
        players: (json['players'] as List)
            ?.map((e) => e == null
                ? null
                : GamePlayer.fromJson((e as Map)?.map(
                    (k, e) => MapEntry(k as String, e),
                  )))
            ?.toList(),
        price: (json['price'] as num)?.toDouble(),
        status: json['status'] as int,
        rating: json['rating'] == null
            ? null
            : Rating.fromJson((json['rating'] as Map)?.map(
                (k, e) => MapEntry(k as String, e),
              )),
        userId: json['user_id'] as String,
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'age_group': ageGroup?.toJson(),
        'booking': GameData._bookFieldDataTodocumentReference(booking),
        'booking_type': bookingType,
        'created_at': AppUtility.dateTimeToString(createdAt),
        'description': description,
        'game_date_time': AppUtility.dateTimeToString(gameDateTime),
        'game_meta': gameMeta?.toJson(),
        'id': id,
        'image': image,
        'is_private': isPrivate,
        'match_type': matchType,
        'name': name,
        'player_needed': playerNeeded,
        'players': players?.map((e) => e?.toJson())?.toList(),
        'price': price,
        'status': status,
        'rating': rating?.toJson(),
        'user_id': userId,
      };

  static BookFieldData _documentReferenceToBookFieldData(
      DocumentReference reference) {
    return new BookFieldData();
  }

  static DocumentReference _bookFieldDataTodocumentReference(
      BookFieldData bookFieldData) {
    return bookFieldData == null
        ? null
        : Firestore.instance
            .collection('field_booking')
            .document(bookFieldData.id);
  }

  double getPlayersJoinedPercentage() {
    if (players != null && players.length > 0) {
      return (playerNeeded / players.length) / 100;
    }
    return 0;
  }
}
