
/// rating but idk for who
/// i think to user
class Rating {
  double totalRating;
  int totalUsers;

  Rating({this.totalRating = 0, this.totalUsers = 0});

  factory Rating.fromJson(Map<String, dynamic> json) => Rating(
        totalRating: (json['total_rating'] as num)?.toDouble(),
        totalUsers: json['total_users'] as int,
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'total_rating': totalRating,
        'total_users': totalUsers,
      };

  double getAverageRating() {
    double average = 0.0;
    try {
      average = totalRating / totalUsers;
    } catch (e) {
      print(e);
    }
    return average > 0 ? double.parse(average.toStringAsFixed(1)) : 0.0;
  }
}
