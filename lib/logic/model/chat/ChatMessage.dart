//import 'package:tqsema/utils/AppUtility.dart';
//
//class ChatMessage {
//  String id;
//  String userId;
//  String userName;
//  String message;
//  DateTime createdAt;
//
//  ChatMessage(
//      {this.id, this.userId, this.userName, this.message, this.createdAt});
//
//  factory ChatMessage.fromJson(Map<String, dynamic> json) => ChatMessage(
//        id: json['id'] as String,
//        userId: json['user_id'] as String,
//        userName: json['userName'] as String,
//        message: json['message'] as String,
//        createdAt:
//            AppUtility.stringToFullDateTime(json['created_at'] as String),
//      );
//
//  Map<String, dynamic> toJson() => <String, dynamic>{
//        'id': id,
//        'user_id': userId,
//        'userName': userName,
//        'message': message,
//        'created_at': AppUtility.fullDateTimeToString(createdAt),
//      };
//}
