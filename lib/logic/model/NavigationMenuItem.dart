import 'package:flutter/material.dart';

class NavigationMenuItem {
  Widget icon;
  String title;
  Widget fragment;

  NavigationMenuItem({this.icon, this.title, this.fragment});
}
