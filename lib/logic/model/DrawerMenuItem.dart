import 'package:flutter/material.dart';

///for menu item
///we need to add function
class DrawerMenuItem {
  int id;
  String title;
  Widget icon;
  List<DrawerMenuItem> children;

  DrawerMenuItem(this.id, this.title,
      {this.icon, this.children = const <DrawerMenuItem>[]});
}
