
/// i think the period of game
class AgeGroup {
  int from;
  int to;

  AgeGroup({this.from, this.to});

  factory AgeGroup.fromJson(Map<String, dynamic> json) => AgeGroup(
        from: json['from'] as int,
        to: json['to'] as int,
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'from': from,
        'to': to,

      };
}
