import 'package:tqsema_final/logic/model/Rating.dart';

class User {
  String uid;
  String phone;
  String name;
  String dob;
  String city;
  String countryName;
  String countryCode;
  String profileImage;
  Rating rating;

  User(
      {this.uid,
      this.name,
      this.phone,
      this.dob,
      this.city,
      this.countryName,
      this.countryCode,
      this.profileImage,
      this.rating}) {
    if (this.rating == null) {
      this.rating = Rating();
    }
  }

  factory User.fromJson(Map<String, dynamic> json) => User(
        uid: json['uid'] as String,
        name: json['name'] as String,
        phone: json['phone'] as String,
        dob: json['dob'] as String,
        city: json['city'] as String,
        countryName: json['countryName'] as String,
        countryCode: json['countryCode'] as String,
        profileImage: json['profileImage'] as String,
        rating: json['rating'] == null
            ? null
            : Rating.fromJson((json['rating'] as Map)?.map(
                (k, e) => MapEntry(k as String, e),
              )),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'uid': uid,
        'phone': phone,
        'name': name,
        'dob': dob,
        'city': city,
        'countryName': countryName,
        'countryCode': countryCode,
        'profileImage': profileImage,
        'rating': rating?.toJson(),
      };
}
