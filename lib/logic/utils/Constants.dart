class Constants {
  static const String app_name = "Tqsema";

  static String FONT_NAME = 'Barlow';

  static String PLEASE_WAIT = 'Please wait...';
  static String ERROR_SOMETHING_WENT_WRONG =
      'Something went wrong! Please try again!';

  static String DATE_FORMAT = 'yyyy-MM-dd';
  static String TIME_FORMAT = 'kk:mm';
  static String DATE_TIME_FORMAT = 'yyyy-MM-dd kk:mm';
  static String FULL_DATE_TIME_FORMAT = 'yyyy-MM-dd kk:mm:ss';

  static String DISPLAY_DATE_FORMAT = 'dd MMM yyyy';
  static String DISPLAY_TIME_FORMAT = 'hh:mm aa';
  static String DISPLAY_DATE_TIME_FORMAT = 'dd MMM yyyy hh:mm aa';

  static String CURRENCY = 'SAR';

  static String PRIVACY_POLICY = 'https://www.google.com';
  static String CONTACT_EMAIL = 'abc@xyz.com';
}
