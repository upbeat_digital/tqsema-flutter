import 'package:get_it/get_it.dart';
import '../../../ui/responsive_ui/base_widget.dart';

GetIt getIt = GetIt.I;
void setup() {
  getIt.registerSingleton<BaseWidget>(BaseWidget());
}
