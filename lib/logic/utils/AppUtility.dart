import 'package:flutter/material.dart';
import 'package:html/parser.dart';
import 'package:intl/intl.dart';

import 'Constants.dart';

class AppUtility {
  showProgressDialog(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return Text('hello');
        });
  }

  showMessageDialog(BuildContext context, String message,
      {String title = 'tqsema',
      String positiveText = 'Okay',
      bool cancelable = true,
      VoidCallback onPressed}) {
    if (onPressed == null) {
      onPressed = () {
        Navigator.pop(context);
      };
    }
    showDialog(
        context: context,
        barrierDismissible: cancelable,
        builder: (context) => AlertDialog(
              title: Text(
                title,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              content: Text(message),
              actions: <Widget>[
                FlatButton(
                  child: Text(
                    positiveText,
                    style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontWeight: FontWeight.bold),
                  ),
                  onPressed: onPressed,
                )
              ],
            ));
  }

  showConfirmDialog(
      BuildContext context, String message, VoidCallback onPressed,
      {String title = 'taqsem',
      String negativeText = 'No',
      String positiveText = 'Yes',
      bool cancelable = true}) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(
          title,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        content: Text(message),
        actions: <Widget>[
          FlatButton(
            child: Text(
              negativeText,
              style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          FlatButton(
            child: Text(
              positiveText,
              style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontWeight: FontWeight.bold),
            ),
            onPressed: onPressed,
          ),
        ],
      ),
    );
  }

  Future<DateTime> getDateFromDatePicker(BuildContext context,
      {DateTime initialDate, DateTime firstDate, DateTime lastDate}) async {
    DateTime currentDate = DateTime.now();
    initialDate = initialDate ?? currentDate;
    firstDate = firstDate ?? DateTime(1900);
    lastDate = lastDate ?? currentDate;
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: initialDate,
      firstDate: firstDate,
      lastDate: lastDate,
    );
    return picked;
  }

  Future<TimeOfDay> getTimeFromTimePicker(BuildContext context,
      {TimeOfDay initialTime}) async {
    TimeOfDay currentTime = TimeOfDay.now();
    initialTime = initialTime ?? currentTime;
    TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: initialTime,
    );
    return picked;
  }

  String fromHtml(String html) {
    return parse(html).documentElement.text;
  }

  static DateTime stringToDateTime(String gameDateTime) {
    return gameDateTime != null
        ? DateFormat(Constants.DATE_TIME_FORMAT).parse(gameDateTime)
        : null;
  }

  static String dateTimeToString(DateTime gameDateTime) {
    return gameDateTime != null
        ? DateFormat(Constants.DATE_TIME_FORMAT).format(gameDateTime)
        : null;
  }

  static DateTime stringToFullDateTime(String gameDateTime) {
    return gameDateTime != null
        ? DateFormat(Constants.FULL_DATE_TIME_FORMAT).parse(gameDateTime)
        : null;
  }

  static String fullDateTimeToString(DateTime gameDateTime) {
    return gameDateTime != null
        ? DateFormat(Constants.FULL_DATE_TIME_FORMAT).format(gameDateTime)
        : null;
  }
}
