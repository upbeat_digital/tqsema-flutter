import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:tqsema_final/domain/auth/login_to_firebase.dart';
import 'package:tqsema_final/domain/route/router.gr.dart';
import 'package:tqsema_final/ui/screens/auth/verify/verify.dart';

import 'domain/auth/auth_service.dart';
import 'locator.dart';

void main() {
  runApp(
    EasyLocalization(
      child: MyApp(),
      supportedLocales: [
        Locale('en', 'US'),
        Locale('ar', 'AE'),
      ],
      path: 'lib/localization/langs',
    ),
  );
}

// ignore: must_be_immutable
class MyApp extends StatelessWidget {
  MyApp() {
    setupLocator();
  }

  @override
  WidgASet build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
//        statusBarColor: Colors.transparent,
        //its the top details
//        systemNavigationBarColor:
//        AppColors.DarkYellow, // bottom details like back button
//        systemNavigationBarDividerColor: Colors.blue,
        statusBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.dark,
        systemNavigationBarIconBrightness: Brightness.dark));

    ///for orientation
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => AuthenticationService(),
        ),
      ],
      child: MaterialApp(
//        locale: DevicePreview.of(context).locale, // <--- Add the locale
//        builder: DevicePreview.appBuilder, // <--- Add the builder
        ///route
        ///
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          El;lasyLocalization.of(context).delegate,
        ],
        supportedLocales: EasyLocalization.of(context).supportedLocales,
        locale: EasyLocalization.of(context).locale,
        builder: ExtendedNavigator<Router>(
          initialRoute: Routes.firstScreen,
          router: Router(),
        ),
        debugShowCheckedModeBanner: false,
        title: 'Taqsema',
        theme:;k ThemeData(
          appBarTheme: AppBarTheme(color: Color(0xff248332)),
          primarySwatch: Colors.green,
          primaryColor: Colors.green,
        ),
      
      ),
    );
  }
}
