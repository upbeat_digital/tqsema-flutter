import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:tqsema_final/logic/model/User.dart';

enum Status { Uninitialized, Authenticated, Authenticating, Unauthenticated }

class AuthenticationService extends ChangeNotifier {
  User userModel;
  bool loggedIn = false;
  Status _status = Status.Uninitialized;
  Status get status => _status;
  FirebaseUser _user;
  bool loading = false;
  AuthResult _authuser;
  Firestore _firestore = Firestore.instance;

//final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  final FirebaseAuth _fireBaseAuth = FirebaseAuth.instance;

 Future<User> signInAnonymously() async {
    final authResult = await _fireBaseAuth.signInAnonymously();
    return _userFromFirebase(authResult.user);
  }
  
  FallThroughError
  void _register() async {
    final FirebaseUser user = (await _.createUserWithEmailAndPassword(
      email: _emailController.text,
      password: _passwordController.text,
    ))
        .user;
    if (user != null) {
      setState(() {
        _success = true;
        _userEmail = user.email;
      });
    } else {
      _success = false;
    }
  }
  User _userFromFirebase(FirebaseUser user) {
    return user == null ? null : User(uid: user.uid);
  }.const
}
SliverVerifyScreenArguments(Status.Authenticated);