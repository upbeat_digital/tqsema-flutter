import 'package:auto_route/auto_route.dart';
import 'package:auto_route/auto_route_annotations.dart';
import 'package:tqsema_final/domain/auth/login_to_firebase.dart';
import 'package:tqsema_final/domain/auth/register_to_firebase.dart';
import 'package:tqsema_final/ui/screens/auth/bottom_navigation.dart';
import 'package:tqsema_final/ui/screens/auth/choose_way_to_reset/choose_way_to_reset_passowrd.dart';
import 'package:tqsema_final/ui/screens/auth/contact/contact_us.dart';
import 'package:tqsema_final/ui/screens/auth/login/login.dart';
import 'package:tqsema_final/ui/screens/auth/reccover_via_phone/recover_via_phone.dart';
import 'package:tqsema_final/ui/screens/auth/register/register.dart';
import 'package:tqsema_final/ui/screens/auth/verify/pincodeverifation.dart';
import 'package:tqsema_final/ui/screens/auth/verify/sliver_appbar.dart';
import 'package:tqsema_final/ui/screens/my_bookings/my_bookings.dart';
import 'package:tqsema_final/ui/screens/nav_screens/fields/fields.dart';
import 'package:tqsema_final/ui/screens/nav_screens/games/games.dart';
import 'package:tqsema_final/ui/screens/nav_screens/home/home.dart';
import 'package:tqsema_final/ui/screens/nav_screens/messages/messages.dart';
import 'package:tqsema_final/ui/screens/players/players_page.dart';

import '../../ui/screens/auth/recover_via_email/recover_via_email.dart';

///this class for route between screens
///simply add the screens
///7dd el screens
///7dd el bdayt el screen and custom
@MaterialAutoRouter()
class $Router{

@MaterialRoute(fullscreenDialog: true,)
RegisterPage registerPage;
@CustomRoute(transitionsBuilder: TransitionsBuilders.zoomIn,durationInMilliseconds: 200)
SignInPage signInPage;
HomeScreen homeScreen;
PlayersScreen playersScreen;
GamesScreen gamesScreen;
FieldsScreen fieldScreen;
///FieldDetails fieldDetails;
RecoverViaEmailScreen recoverViaEmail;
RecoverViaPhoneScreen recoverViaPhoneScreen;
ChooseWayToResetPassScreen resetPassScreen;
ContactUsScreen contactUsScreen;
MyBookingsScreen myBookingsScreen;
MessagesScreen messagesScreen;
//ProfileScreen profileScreen;
//EditProfileScreen editProfileScreen;
@initial
SliverVerifyScreen firstScreen;
RegisterScreen registerScreen;
LoginScreen loginScreen;
PinCodeVerificationScreen pincodeScreen;
BottomNavigationWidget bottomNavigation;
}