// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/auto_route.dart';
import 'package:tqsema_final/domain/auth/register_to_firebase.dart';
import 'package:tqsema_final/domain/auth/login_to_firebase.dart';
import 'package:tqsema_final/ui/screens/nav_screens/home/home.dart';
import 'package:tqsema_final/ui/screens/players/players_page.dart';
import 'package:tqsema_final/ui/screens/nav_screens/games/games.dart';
import 'package:tqsema_final/ui/screens/nav_screens/fields/fields.dart';
import 'package:tqsema_final/ui/screens/auth/recover_via_email/recover_via_email.dart';
import 'package:tqsema_final/ui/screens/auth/reccover_via_phone/recover_via_phone.dart';
import 'package:tqsema_final/ui/screens/auth/choose_way_to_reset/choose_way_to_reset_passowrd.dart';
import 'package:tqsema_final/ui/screens/auth/contact/contact_us.dart';
import 'package:tqsema_final/ui/screens/my_bookings/my_bookings.dart';
import 'package:tqsema_final/ui/screens/nav_screens/messages/messages.dart';
import 'package:tqsema_final/ui/screens/auth/verify/sliver_appbar.dart';
import 'package:tqsema_final/ui/screens/auth/register/register.dart';
import 'package:tqsema_final/ui/screens/auth/login/login.dart';
import 'package:tqsema_final/ui/screens/auth/verify/pincodeverifation.dart';
import 'package:tqsema_final/ui/screens/auth/bottom_navigation.dart';

abstract class Routes {
  static const registerPage = '/register-page';
  static const signInPage = '/sign-in-page';
  static const homeScreen = '/home-screen';
  static const playersScreen = '/players-screen';
  static const gamesScreen = '/games-screen';
  static const fieldScreen = '/field-screen';
  static const recoverViaEmail = '/recover-via-email';
  static const recoverViaPhoneScreen = '/recover-via-phone-screen';
  static const resetPassScreen = '/reset-pass-screen';
  static const contactUsScreen = '/contact-us-screen';
  static const myBookingsScreen = '/my-bookings-screen';
  static const messagesScreen = '/messages-screen';
  static const profileScreen = '/profile-screen';
  static const editProfileScreen = '/edit-profile-screen';
  static const firstScreen = '/';
  static const registerScreen = '/register-screen';
  static const loginScreen = '/login-screen';
  static const pincodeScreen = '/pincode-screen';
  static const bottomNavigation = '/bottom-navigation';
}

class Router extends RouterBase {
  //This will probably be removed in future versions
  //you should call ExtendedNavigator.ofRouter<Router>() directly
  static ExtendedNavigatorState get navigator =>
      ExtendedNavigator.ofRouter<Router>();

  @override
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Routes.registerPage:
        return MaterialPageRoute<dynamic>(
          builder: (_) => RegisterPage(),
          settings: settings,
          fullscreenDialog: true,
        );
      case Routes.signInPage:
        return PageRouteBuilder<dynamic>(
          pageBuilder: (ctx, animation, secondaryAnimation) => SignInPage(),
          settings: settings,
          transitionsBuilder: TransitionsBuilders.zoomIn,
          transitionDuration: const Duration(milliseconds: 200),
        );
      case Routes.homeScreen:
        return MaterialPageRoute<dynamic>(
          builder: (_) => HomeScreen(),
          settings: settings,
        );
      case Routes.playersScreen:
        return MaterialPageRoute<dynamic>(
          builder: (_) => PlayersScreen(),
          settings: settings,
        );
      case Routes.gamesScreen:
        return MaterialPageRoute<dynamic>(
          builder: (_) => GamesScreen(),
          settings: settings,
        );
      case Routes.fieldScreen:
        return MaterialPageRoute<dynamic>(
          builder: (_) => FieldsScreen(),
          settings: settings,
        );
      case Routes.recoverViaEmail:
        if (hasInvalidArgs<RecoverViaEmailScreenArguments>(args)) {
          return misTypedArgsRoute<RecoverViaEmailScreenArguments>(args);
        }
        final typedArgs = args as RecoverViaEmailScreenArguments ??
            RecoverViaEmailScreenArguments();
        return MaterialPageRoute<dynamic>(
          builder: (_) => RecoverViaEmailScreen(key: typedArgs.key),
          settings: settings,
        );
      case Routes.recoverViaPhoneScreen:
        if (hasInvalidArgs<RecoverViaPhoneScreenArguments>(args)) {
          return misTypedArgsRoute<RecoverViaPhoneScreenArguments>(args);
        }
        final typedArgs = args as RecoverViaPhoneScreenArguments ??
            RecoverViaPhoneScreenArguments();
        return MaterialPageRoute<dynamic>(
          builder: (_) => RecoverViaPhoneScreen(key: typedArgs.key),
          settings: settings,
        );
      case Routes.resetPassScreen:
        if (hasInvalidArgs<ChooseWayToResetPassScreenArguments>(args)) {
          return misTypedArgsRoute<ChooseWayToResetPassScreenArguments>(args);
        }
        final typedArgs = args as ChooseWayToResetPassScreenArguments ??
            ChooseWayToResetPassScreenArguments();
        return MaterialPageRoute<dynamic>(
          builder: (_) => ChooseWayToResetPassScreen(key: typedArgs.key),
          settings: settings,
        );
      case Routes.contactUsScreen:
        if (hasInvalidArgs<ContactUsScreenArguments>(args)) {
          return misTypedArgsRoute<ContactUsScreenArguments>(args);
        }
        final typedArgs =
            args as ContactUsScreenArguments ?? ContactUsScreenArguments();
        return MaterialPageRoute<dynamic>(
          builder: (_) => ContactUsScreen(key: typedArgs.key),
          settings: settings,
        );
      case Routes.myBookingsScreen:
        if (hasInvalidArgs<MyBookingsScreenArguments>(args)) {
          return misTypedArgsRoute<MyBookingsScreenArguments>(args);
        }
        final typedArgs =
            args as MyBookingsScreenArguments ?? MyBookingsScreenArguments();
        return MaterialPageRoute<dynamic>(
          builder: (_) => MyBookingsScreen(key: typedArgs.key),
          settings: settings,
        );
      case Routes.messagesScreen:
        return MaterialPageRoute<dynamic>(
          builder: (_) => MessagesScreen(),
          settings: settings,
        );
      // case Routes.profileScreen:
      //   return MaterialPageRoute<dynamic>(
      //     builder: (_) => ProfileScreen(),
      //     settings: settings,
      //   );
      // case Routes.editProfileScreen:
      //   return MaterialPageRoute<dynamic>(
      //     builder: (_) => EditProfileScreen(),
      //     settings: settings,
      //   );
      case Routes.firstScreen:
        if (hasInvalidArgs<SliverVerifyScreenArguments>(args)) {
          return misTypedArgsRoute<SliverVerifyScreenArguments>(args);
        }
        final typedArgs = args as SliverVerifyScreenArguments ??
            SliverVerifyScreenArguments();
        return MaterialPageRoute<dynamic>(
          builder: (_) => SliverVerifyScreen(key: typedArgs.key),
          settings: settings,
        );
      case Routes.registerScreen:
        return MaterialPageRoute<dynamic>(
          builder: (_) => RegisterScreen(),
          settings: settings,
        );
      case Routes.loginScreen:
        return MaterialPageRoute<dynamic>(
          builder: (_) => LoginScreen(),
          settings: settings,
        );
      case Routes.pincodeScreen:
        if (hasInvalidArgs<PinCodeVerificationScreenArguments>(args)) {
          return misTypedArgsRoute<PinCodeVerificationScreenArguments>(args);
        }
        final typedArgs = args as PinCodeVerificationScreenArguments ??
            PinCodeVerificationScreenArguments();
        return MaterialPageRoute<dynamic>(
          builder: (_) =>
              PinCodeVerificationScreen(phoneNumber: typedArgs.phoneNumber),
          settings: settings,
        );
      case Routes.bottomNavigation:
        return MaterialPageRoute<dynamic>(
          builder: (_) => BottomNavigationWidget(),
          settings: settings,
        );
      default:
        return unknownRoutePage(settings.name);
    }
  }
}

//**************************************************************************
// Arguments holder classes
//***************************************************************************

//RecoverViaEmailScreen arguments holder class
class RecoverViaEmailScreenArguments {
  final Key key;
  RecoverViaEmailScreenArguments({this.key});
}

//RecoverViaPhoneScreen arguments holder class
class RecoverViaPhoneScreenArguments {
  final Key key;
  RecoverViaPhoneScreenArguments({this.key});
}

//ChooseWayToResetPassScreen arguments holder class
class ChooseWayToResetPassScreenArguments {
  final Key key;
  ChooseWayToResetPassScreenArguments({this.key});
}

//ContactUsScreen arguments holder class
class ContactUsScreenArguments {
  final Key key;
  ContactUsScreenArguments({this.key});
}

//MyBookingsScreen arguments holder class
class MyBookingsScreenArguments {
  final Key key;
  MyBookingsScreenArguments({this.key});
}

//SliverVerifyScreen arguments holder class
class SliverVerifyScreenArguments {
  final Key key;
  SliverVerifyScreenArguments({this.key});
}

//PinCodeVerificationScreen arguments holder class
class PinCodeVerificationScreenArguments {
  final String phoneNumber;
  PinCodeVerificationScreenArguments({this.phoneNumber});
}
