import 'package:flutter/material.dart';

// ignore: must_be_immutable
class NavScreenImage extends StatelessWidget {

  String imageRes='https://i.ytimg.com/vi/aZCLl_2nJDI/maxresdefault.jpg';
  double width;
  double height;

  NavScreenImage(String dummyImage, {this.width,this.height});
  @override
  Widget build(BuildContext context) {

    return ClipRRect(
//      clipBehavior: Clip.antiAlias,
      borderRadius: BorderRadius.circular(15),
      child: Image.network(

        imageRes,
        width: width,
        height: height,
        fit: BoxFit.cover,
      ),
    );
  }
}
