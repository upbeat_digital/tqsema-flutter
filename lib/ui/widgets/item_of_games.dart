import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

import 'Theme_and_colors.dart';
import 'image_alot_at_main_with_title.dart';

// ignore: must_be_immutable
class ItemOfGame extends StatelessWidget {
  String image;

  ItemOfGame({this.image});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      clipBehavior: Clip.antiAlias,
      child: Stack(
        children: <Widget>[
          ListTile(
            leading:SizedBox(
              width: 100,
            ),
                            //name of the game
            title: Text('Aein Game'),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      // date game
                      Text('5 Feb 2020',style: TextStyle(fontSize: 12/(MediaQuery.of(context).textScaleFactor)),),
                      //price the game
                      Text('35 AAD',style: TextStyle(fontSize: 12/(MediaQuery.of(context).textScaleFactor)),),

                    ],
                  ),
                ),
                LinearPercentIndicator(
                  lineHeight: 25.0,
                  percent: .9,
                  center: Text(
                    // 3dd el players
                    '9 player joined',
                    style: TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold),
                  ),
                  progressColor: AppColors.accentColor,

                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: Colors.white,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Join Now',
                      style: TextStyle(color: Colors.green),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TitledNavScreenImage(
              image,
              txt: 'Monday, 10am',
              width: 100,
              height: 100,
            ),
          ),
        ],
      ),
    );
  }
}