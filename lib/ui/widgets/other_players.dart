// ignore: must_be_immutable
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'player_stars.dart';
///player section in home screen
///static must be dynamic
///the name and stars and pic
/// el mfrod hna ageeb el data mn el firebase

// ignore: must_be_immutable
class OtherPlayers extends StatelessWidget {
  String dummyImage;
  Function openPlayerInfo;

  OtherPlayers({this.dummyImage,this.openPlayerInfo});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(25.0),
//        borderRadius: BorderRadius.circular(20),
//        shape: BoxShape.circle
      ),
      child: Card(

        child: Padding(
          padding: const EdgeInsets.all(2.0),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(2.0),
                child: InkWell(
                  onTap: openPlayerInfo,
                  splashColor: Colors.green,
                  child: CircleAvatar(
                    backgroundImage: AssetImage('images/m.jpg'),
                    radius: 25,
                  ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text('Mohamed Reda'),
                  PlayerStars(itemSize: 10,
                    color: Colors.green,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
