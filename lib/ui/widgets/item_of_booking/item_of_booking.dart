
// ignore: must_be_immutable
import 'package:flutter/material.dart';

import '../image_alot_at_main.dart';

// ignore: must_be_immutable
class ItemOfBooking extends StatelessWidget {
  String image;

  ItemOfBooking({this.image});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      clipBehavior: Clip.antiAlias,
      child: Stack(
        children: <Widget>[
          ListTile(
            leading: SizedBox(
              width: 100,
            ),
            title: Text('Emirates Court'),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.location_on,
                        color: Colors.green,
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * .50,
                        child: Text(
                          'Box No. 31420 , Dubai , Emirates',
                          style: TextStyle(fontSize: 12),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.date_range,
                        color: Colors.green,
                      ),
                      Text(
                        '5 WED 10:00 am',
                        style: TextStyle(fontSize: 12),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: NavScreenImage(image, width: 110, height: 100),
          ),
        ],
      ),
    );
  }
}