import 'package:flutter/material.dart';

class CustomeAppBar extends StatefulWidget implements PreferredSizeWidget {
  final String text;

   final double contentHeight = 55.0;
  

  CustomeAppBar({
    this.text,
    
   
  }) : super();

  @override
  State<StatefulWidget> createState() {
    return new _CustomeAppBarState(
    
      this.text,
      
      );
  }

  @override
  Size get preferredSize => new Size.fromHeight(
    
    contentHeight
    
    );
}
class _CustomeAppBarState extends State<CustomeAppBar> {
  String text;

  _CustomeAppBarState(this.text);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      elevation: 0,
      title: Text(text),
      backgroundColor: Colors.green,
      leading: IconButton(
        icon: Icon(
          Icons.menu,
          color: Colors.white,
        ),
        onPressed: () {
           Scaffold.of(context).openDrawer();
        },
      ),
    );
  }
}
