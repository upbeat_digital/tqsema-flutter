import 'package:flutter/material.dart';

// ignore: must_be_immutable
class AboutFieldRowHaveIcon extends StatelessWidget {
  IconData icon;
  String aboutText;

  AboutFieldRowHaveIcon({this.icon, this.aboutText});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        CircleAvatar(
          child: Icon(
            icon,
          ),
          backgroundColor: Colors.green,
          radius: 16,
        ),
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Text(aboutText),
        ),
      ],
    );
  }
}
