// ignore: must_be_immutable
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ColumnOfFieldDetails extends StatelessWidget {
  String title;
  String count;
  IconData icon;

  ColumnOfFieldDetails({this.title, this.count, this.icon});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(title),
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Icon(
            icon,
            color: Colors.green,
          ),
        ),
        Text(count),
      ],
    );
  }
}
