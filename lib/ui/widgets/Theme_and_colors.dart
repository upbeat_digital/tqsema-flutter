import 'package:flutter/material.dart';
import '../../logic/utils/Constants.dart';

final ThemeData AppThemeData = new ThemeData(
  primaryColor: AppColors.primaryColor,
  primaryColorDark: AppColors.primaryColorDark,
  accentColor: AppColors.accentColor,
  primarySwatch: Colors.green,
  fontFamily: Constants.FONT_NAME,
);

class AppColors {
  AppColors._();

  static const Color primaryColor = Color(0xFF009444);
  static const Color primaryColorDark = Color(0xFF000000);
  static const Color accentColor = Color(0xFFEB7B43);

  static const Color appBlueColor = Color(0xFF313131);
  static const Color appBlackColor = Color(0xFF313131);
  static const Color appGreyColor = Color(0xFF959595);
  static const Color appLightGreyColor = Color(0xFFE1E1E1);

  static const Color facebookBlueColor = Color(0xFF4266b2);

  static const Color myChatBgColor = Color(0xFFCFF1B1);
}
