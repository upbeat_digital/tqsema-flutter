import 'package:flutter/material.dart';

import 'image_alot_at_main.dart';

// ignore: must_be_immutable
class TitledNavScreenImage extends StatelessWidget {

  String txt;
  double width;
  double height;
  String dummyImage;

  TitledNavScreenImage(this.dummyImage, {this.txt,this.width,this.height});
  @override
  Widget build(BuildContext context) {

    return Container(
      width: width,
      height: height,
      child: Stack(
        children: <Widget>[
          NavScreenImage(dummyImage,width: width,height: height),
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: Colors.green),
              child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: Text(
                  txt,
                  style: TextStyle(color: Colors.white,fontSize: 8),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
