
import 'package:flutter/material.dart';

import '../player_stars.dart';

///for player card info name , image country , position

class PlayerCard extends StatelessWidget {
  PlayerCard({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            SizedBox(height: 8,),
            CircleAvatar(
              // hnrg3 hna mn firebase
              backgroundImage: AssetImage('images/m.jpg'),
              radius: 25,
            ),
            Padding(
              //hnrg3 hna mn firebase el name
              padding: const EdgeInsets.all(4.0),
              child: Text('Mohamed Reda'),
            ),
            Padding(
              //hnrg3 hna mn firebase position
              padding: const EdgeInsets.only(bottom:8.0),
              child: Text('Center Back'),
            ),
            SizedBox(height: 16,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                //hnrg3 hna mn el firebase el location
                Text('Dubai',style: TextStyle(color: Colors.grey),),
                // stars of rating
                PlayerStars(itemSize: 12.0,),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
