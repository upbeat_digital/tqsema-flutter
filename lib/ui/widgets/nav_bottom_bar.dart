//import 'package:flutter/material.dart';
//import 'package:tqsema_final/ui/screens/my_bookings/my_bookings.dart';
//import 'package:tqsema_final/ui/screens/nav_screens/fields/fields.dart';
//import 'package:tqsema_final/ui/screens/nav_screens/games/games.dart';
//import 'package:tqsema_final/ui/screens/nav_screens/home/home.dart';
//import 'package:tqsema_final/ui/screens/nav_screens/messages/messages.dart';
//import '../screens/holding_screen_provider.dart';
//import '../screens/nav_screens/fields/fields.dart';
//import '../screens/nav_screens/games/games.dart';
//import '../screens/nav_screens/home/home.dart';
//import '../screens/nav_screens/messages/messages.dart';
//
//class MyBottomNavigationBar extends StatefulWidget {
//  ProviderOfHoldingScreen providerOfHold;
//  MyBottomNavigationBar({this.providerOfHold});
//  static int _index = 0;
//
//  static getIndex() => _index;
//
//  @override
//  _MyBottomNavigationBarState createState() => _MyBottomNavigationBarState();
//}
//
//class _MyBottomNavigationBarState extends State<MyBottomNavigationBar> {
//  static int _index = 0;
//
//  List<Widget> navigationClasses = [
//    HomeScreen(),
//    FieldsScreen(),
//    GamesScreen(),
//    MessagesScreen(),
//  ];
//  Widget currentScreen = HomeScreen();
//
//  static getIndex() => _index;
//  setIndex(int index) {
//    setState(() {
//      MyBottomNavigationBar._index = index;
//
//      currentScreen = navigationClasses[index];
//    });
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return BottomNavigationBar(
//      iconSize: 30,
//      selectedItemColor: Colors.green,
//      currentIndex: getIndex(),
//      unselectedItemColor: Colors.grey,
//      backgroundColor: Colors.white,
//      unselectedLabelStyle: TextStyle(color: Colors.grey),
//      showUnselectedLabels: true,
//      onTap: (int index) {
//        setIndex(index);
//      },
//
//      items: <BottomNavigationBarItem>[
//        BottomNavigationBarItem(
//          icon: ImageIcon(
//            AssetImage("images/icon_home.png"),
//          ),
//          title: Text('Home'),
//        ),
//        BottomNavigationBarItem(
//          icon: ImageIcon(
//            AssetImage("images/fields_icon.png"),
//          ),
//          title: Text('Fields'),
//        ),
//        BottomNavigationBarItem(
//          icon: ImageIcon(
//            AssetImage("images/game_icon.png"),
//          ),
//          title: Text('Games'),
//        ),
//        BottomNavigationBarItem(
//          icon: ImageIcon(
//            AssetImage("images/icon_message.png"),
//          ),
//          title: Text('Messages'),
//        ),
//      ],
//    );
//  }
//}
