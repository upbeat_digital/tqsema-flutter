

import 'package:calendar_strip/calendar_strip.dart';
import 'package:flutter/material.dart';
import 'package:horizontal_calendar_widget/date_helper.dart';
import 'package:horizontal_calendar_widget/horizontal_calendar.dart';
class CalendarWidget extends StatefulWidget {
  @override
  _CalendarWidgetState createState() => _CalendarWidgetState();
}
const labelMonth = 'Month';
const labelDate = 'Date';
const labelWeekDay = 'Week Day';

class _CalendarWidgetState extends State<CalendarWidget> {
  DateTime startDate = DateTime.now().subtract(Duration(days: 2));
  DateTime endDate = DateTime.now().add(Duration(days: 2));
  DateTime selectedDate = DateTime.now().subtract(Duration(days: 2));
  List<DateTime> markedDates = [
    DateTime.now().subtract(Duration(days: 1)),
    DateTime.now().subtract(Duration(days: 2)),
    DateTime.now().add(Duration(days: 4))
  ];

  onSelect(data) {
    print("Selected Date -> $data");
  }

  _monthNameWidget(monthName) {
    return Container(
      child: Text(monthName,
          style:
          TextStyle(fontSize: 17,
              fontWeight: FontWeight.w600,
              color: Colors.black87,
              fontStyle: FontStyle.italic)),
      padding: EdgeInsets.only(top: 8, bottom: 4),
    );
  }

  getMarkedIndicatorWidget() {
    return Row(mainAxisAlignment: MainAxisAlignment.center, children: [
      Container(
        margin: EdgeInsets.only(left: 1, right: 1),
        width: 7,
        height: 7,
        decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.red),
      ),
      Container(
        width: 7,
        height: 7,
        decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.blue),
      )
    ]);
  }

  dateTileBuilder(date, selectedDate, rowIndex, dayName, isDateMarked,
      isDateOutOfRange) {
    bool isSelectedDate = date.compareTo(selectedDate) == 0;
    Color fontColor = isDateOutOfRange ? Colors.black26 : Colors.black87;
    TextStyle normalStyle = TextStyle(
        fontSize: 17, fontWeight: FontWeight.w800, color: fontColor);
    TextStyle selectedStyle = TextStyle(
        fontSize: 17, fontWeight: FontWeight.w800, color: Colors.black87);
    TextStyle dayNameStyle = TextStyle(fontSize: 14.5, color: fontColor);
    List<Widget> _children = [
      Text(dayName, style: dayNameStyle),
      Text(date.day.toString(),
          style: !isSelectedDate ? normalStyle : selectedStyle),
    ];

    if (isDateMarked == true) {
      _children.add(getMarkedIndicatorWidget());
    }

    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      alignment: Alignment.center,
      padding: EdgeInsets.only(top: 8, left: 5, right: 5, bottom: 5),
      decoration: BoxDecoration(
        color: !isSelectedDate ? Colors.transparent : Colors.white70,
        borderRadius: BorderRadius.all(Radius.circular(60)),
      ),
      child: Column(
        children: _children,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        SizedBox(height: 16),
//        HorizontalCalendar(
//          key: forceRender ? UniqueKey() : Key('Calendar'),
//          height: 120,
//          padding: EdgeInsets.all(22),
//          firstDate: firstDate,
//          lastDate: lastDate,
//          dateFormat: dateFormat,
//          weekDayFormat: weekDayFormat,
//          monthFormat: monthFormat,
//          defaultDecoration: BoxDecoration(
//            color: defaultDecorationColor,
//            shape: defaultDecorationShape,
//            borderRadius: defaultDecorationShape == BoxShape.rectangle &&
//                isCircularRadiusDefault
//                ? BorderRadius.circular(8)
//                : null,
//          ),
//          selectedDecoration: BoxDecoration(
//            color: selectedDecorationColor,
//            shape: selectedDecorationShape,
//            borderRadius: selectedDecorationShape == BoxShape.rectangle &&
//                isCircularRadiusSelected
//                ? BorderRadius.circular(8)
//                : null,
//          ),
//          disabledDecoration: BoxDecoration(
//            color: disabledDecorationColor,
//            shape: disabledDecorationShape,
//            borderRadius: disabledDecorationShape == BoxShape.rectangle &&
//                isCircularRadiusDisabled
//                ? BorderRadius.circular(8)
//                : null,
//          ),
//          isDateDisabled: (date) => date.weekday == 7,
//          labelOrder: order.map(toLabelType).toList(),
//          maxSelectedDateCount: maxSelectedDateCount,
//        ),
        Container(
            child: CalendarStrip(
              startDate: startDate,
              endDate: endDate,
              onDateSelected: onSelect,
              dateTileBuilder: dateTileBuilder,
              iconColor: Colors.black87,
              monthNameWidget: _monthNameWidget,
              markedDates: markedDates,
              containerDecoration: BoxDecoration(color: Colors.white70),
            ))


      ],
    );
  }

//
//  void showError(String message) {
//    Scaffold.of(context).showSnackBar(
//      SnackBar(content: Text(message)),
//    );
//  }
//}
//
//Future<DateTime> datePicker(
//    BuildContext context,
//    DateTime initialDate,
//    ) async {
//  final selectedDate = await showDatePicker(
//    context: context,
//    initialDate: initialDate,
//    firstDate: DateTime.now().add(
//      Duration(days: -365),
//    ),
//    lastDate: DateTime.now().add(
//      Duration(days: 365),
//    ),
//  );
//  return toDateMonthYear(selectedDate);
//}
//
//LabelType toLabelType(String label) {
//  LabelType type;
//  switch (label) {
//    case labelMonth:
//      type = LabelType.month;
//      break;
//    case labelDate:
//      type = LabelType.date;
//      break;
//    case labelWeekDay:
//      type = LabelType.weekday;
//      break;
//  }
//  return type;
//}
//
//String fromLabelType(LabelType label) {
//  String labelString;
//  switch (label) {
//    case LabelType.month:
//      labelString = labelMonth;
//      break;
//    case LabelType.date:
//      labelString = labelDate;
//      break;
//    case LabelType.weekday:
//      labelString = labelWeekDay;
//      break;
//  }
//  return labelString;
//}
//
}
