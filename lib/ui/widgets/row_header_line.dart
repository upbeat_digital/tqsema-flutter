import 'package:flutter/material.dart';

// ignore: must_be_immutable

///for header in the home contain  two text
///hn5li fee function 3lashn kd hia static
class HomeHeaderRow extends StatelessWidget {
  String bold = '';
  String green = '';
  bool small;
  Function function;

  HomeHeaderRow({this.bold, this.green, this.small = false,this.function});

  @override
  Widget build(BuildContext context) {
    return small
        ? theRow()
        : Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 6.0, horizontal: 12.0),
            child: theRow(),
          );
  }

  Widget theRow() {
    return Row(
      mainAxisSize: small?MainAxisSize.min:MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          bold,
          style:
              TextStyle(fontSize: small ? 9 : 16, fontWeight: small ?FontWeight.normal:FontWeight.bold),
        ),
        SizedBox(
          width: small?8:0,
        ),
        InkWell(
          onTap: function,
          child: Text(
            green,
            style: TextStyle(color: Colors.green, fontSize: small ? 7 : 12),
          ),
        ),
      ],
    );
  }
}
