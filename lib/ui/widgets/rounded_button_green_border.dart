import 'package:flutter/material.dart';

// ignore: must_be_immutable
class RoundedButtonWithGreenBorder extends StatelessWidget {
  String buttonName;
  Function onPress;

  RoundedButtonWithGreenBorder({this.buttonName,this.onPress});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
          border: Border.all(
            width: 1,
            color: Colors.green,
            style: BorderStyle.solid,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: InkWell(
            onTap: onPress,
            child: Text(
              buttonName,
              style: TextStyle(
                color: Colors.green,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
