import 'package:flutter/material.dart';

// ignore: must_be_immutable
class GreenButton extends StatelessWidget {
  String buttonName = '';
  dynamic function;
  Color buttonColor;

  GreenButton({this.buttonName, this.function,this.buttonColor});

  @override
  Widget build(BuildContext context) {
    return Container(
//        decoration: box??null,
      width: MediaQuery.of(context).size.width * .8,
      height: 50,
      child: RaisedButton(
        elevation: 5,
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(30.0),
        ),
        color: buttonColor??Colors.green,
//      padding: EdgeInsets.fromLTRB(36, 9, 36, 9),
        onPressed: () {
          function();
        },
        child: Text(
          buttonName,
          style: TextStyle(
            color: Colors.white,
            fontSize: 18 / (MediaQuery.of(context).textScaleFactor),
          ),
        ),
      ),
    );
  }
}
