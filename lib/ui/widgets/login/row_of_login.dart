import 'package:flutter/material.dart';

// ignore: must_be_immutable
class RowOfTheTopLogin extends StatelessWidget {
  String headerName='';
  Color color;
  RowOfTheTopLogin({this.headerName,this.color});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal:5.0,vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          IconButton(
            onPressed: (){},
            icon: Icon(Icons.arrow_back_ios,color: color,
            size: 27,
            ),
          ),
          Text(
            headerName,
            style: TextStyle(
                color: color,
                fontSize: 22
            ),
          ),
          SizedBox(width: 50,),
        ],
      ),
    );
  }
}
