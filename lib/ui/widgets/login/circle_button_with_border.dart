// ignore: must_be_immutable
import 'package:flutter/material.dart';

class CircleButtonWithBorder extends StatelessWidget {
  String buttonName;
  IconData iconData;
  Function onPress;

  CircleButtonWithBorder({this.onPress, this.buttonName, this.iconData});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        width: MediaQuery.of(context).size.width * .8,
        height: 50,
        decoration: BoxDecoration(
            color: Colors.green,
            borderRadius: BorderRadius.all(Radius.circular(40)),
            border: Border.all(
                width: 1, color: Colors.white, style: BorderStyle.solid)),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              InkWell(
                onTap: onPress,
                child: Padding(
                  padding: const EdgeInsets.only(
                    right: 12.0,
                    left: 8,
                  ),
                  child: Icon(
                    iconData,
                    color: Colors.white,
                  ),
                ),
              ),
              Text(
                buttonName,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18 / (MediaQuery.of(context).textScaleFactor),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
