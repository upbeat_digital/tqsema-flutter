import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class TextFieldOfLogin extends StatelessWidget {
  BuildContext context;
  String hint;
  double i;
  String ex;
  bool pass;
  FocusNode node;
  FocusNode nextNode;
  dynamic visible;
  Image image;

  TextEditingController controller;

  TextFieldOfLogin({
    this.context,
    this.hint,
    this.i,
    this.ex,
    this.pass,
    this.node,
    this.nextNode,
    this.visible,
    this.controller,
    this.image,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 50.0, vertical: 18),
      child: TextFormField(
        
        validator: (String value) {
          if (value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        controller: controller,
        textAlign: image == null ? TextAlign.center : TextAlign.left,
        focusNode: node,
        onFieldSubmitted: (String value) {
//          if(nextNode!=null){
          FocusScope.of(context).requestFocus(nextNode);
//          }
        },
        cursorColor: Colors.green,
        decoration: InputDecoration(
            prefixIcon: image == null
                ? null
                : 
               Row(
                      mainAxisSize: MainAxisSize.min,
                     crossAxisAlignment: CrossAxisAlignment.center,
                     // mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[image, Text('  +977 ')],
                    ),
                
            prefix: pass != null
                ? InkWell(
                    child: Icon(
                      Icons.remove_red_eye,
                      color: pass ? Colors.grey : Colors.red,
                    ),
                    onTap: () {
//                      visible();
                    },
                  )
                : null,
            hintStyle: new TextStyle(color: Colors.grey[500]),
            labelStyle: new TextStyle(color: Colors.grey[900]),
            hintText: ex ?? hint,
            fillColor: Colors.black),
//        textAlign: MyApp.lang == true ? TextAlign.left : TextAlign.right,
//        textDirection:
//            MyApp.lang == true ? TextDirection.ltr : TextDirection.rtl,
//      validator: (value) {
//            return validate(value);
//      },
        obscureText: pass ?? false,
//          controller: controller,
        keyboardType: image == null ? TextInputType.text : TextInputType.phone,
      ),
    );
  }
}
