import 'package:flutter/material.dart';

import 'row_of_login.dart';

// ignore: must_be_immutable
class TheGreenPartOfTheLogin extends StatelessWidget {
  IconData myIcon;
  String title='';
  TheGreenPartOfTheLogin({this.myIcon,this.title}) ;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Expanded(
      flex: 1,
      child: Container(
        color: Colors.green,
        child: SafeArea(
          child: Column(
            children: <Widget>[
              RowOfTheTopLogin(headerName: title,
              color: Colors.white,
              ),
              Padding(
                padding: const EdgeInsets.all(13.0),
                child: CircleAvatar(
                  radius: 40,
                  backgroundColor: Colors.white,
                  child: Icon(myIcon,size: 60,),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
