import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

/// ** stars for player in home screen **
/// static brdo
///
// ignore: must_be_immutable
class PlayerStars extends StatelessWidget {
  double itemSize = 20;
  Color color = Colors.green;
  PlayerStars({this.itemSize, this.color});
  @override
  Widget build(BuildContext context) {
    return RatingBar(
      itemSize: itemSize,
      initialRating: 4,
//      glowColor: Colors.green,
      minRating: 1,
      direction: Axis.horizontal,
      allowHalfRating: true,
      itemCount: 5,
//      unratedColor: Colors.green[800],
      itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
      itemBuilder: (context, _) => Icon(
        Icons.star,
        color: Colors.green,
      ),
      onRatingUpdate: (rating) {
        print(rating);
      },
    );
  }
}
