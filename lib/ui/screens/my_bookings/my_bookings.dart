import 'package:flutter/material.dart';
import '../../widgets/item_of_booking/item_of_booking.dart';

class MyBookingsScreen extends StatelessWidget {
  MyBookingsScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String dummyImage = 'https://i.ytimg.com/vi/aZCLl_2nJDI/maxresdefault.jpg';
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.arrow_back_ios),
        title: Text('My Booking'),
        centerTitle: true,
      ),
      ///hn3ml hna custom litsview we nrg3 mn firebase
      body: ListView(
        children: <Widget>[
          ItemOfBooking(
            image: dummyImage,
          ),
          ItemOfBooking(
            image: dummyImage,
          ),
          ItemOfBooking(
            image: dummyImage,
          ),
          ItemOfBooking(
            image: dummyImage,
          ),
          ItemOfBooking(
            image: dummyImage,
          ),
        ],
      ),
    );
  }
}
