import 'package:flutter/material.dart';
import 'package:tqsema_final/domain/route/router.gr.dart';
import '../widgets/player_stars.dart';
import '../widgets/rounded_button_green_border.dart';

///drawer for the main screens
///home , messages , games , fields
class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          DrawerHeader(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Refaat taha',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'UAE',
                            ),
                          ),
                          Row(
                            children: <Widget>[
                              PlayerStars(),
//                              //todo common with home
//                              RatingBar(
//                                itemSize: 20,
//                                initialRating: 4,
//                                minRating: 1,
//                                direction: Axis.horizontal,
//                                allowHalfRating: true,
//                                itemCount: 5,
//                                itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
//                                itemBuilder: (context, _) => Icon(
//                                  Icons.star,
//                                  color: Colors.amber,
//                                ),
//                                onRatingUpdate: (rating) {
//                                  print(rating);
//                                },
//                              ),
                              Text(
                                '(4.3)',
                              ),
                            ],
                          ),
                        ],
                      ),
                      Column(

                        children: <Widget>[
                          CircleAvatar(

                            backgroundImage: AssetImage('images/m.jpg'),

                          ),
                          RoundedButtonWithGreenBorder(
                              buttonName: 'edite Profile',
                            onPress: (){
                                Router.navigator.pushNamed(Routes.editProfileScreen);
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Column(
//            physics: NeverScrollableScrollPhysics(),
            children: <Widget>[
              ListTileOfDrawer(
                leading: Icons.home,
                itemOfDrawerName: 'Home',
                onPressed: (){
                  Router.navigator.pushNamed(Routes.bottomNavigation);
                },
                // Some Code
              ),
              ListTileOfDrawer(
                leading: Icons.person_outline,
                itemOfDrawerName: 'Profile',
                onPressed: (){
                  Router.navigator.pushNamed(Routes.profileScreen);
                },
                // Some Code
              ),
              ListTileOfDrawer(
                leading: Icons.filter_none,
                itemOfDrawerName: 'My Booking',
                onPressed: (){
                  Router.navigator.pushNamed(Routes.myBookingsScreen);
                },
                // Some Code
              ),
            ],
          ),
          Divider(

          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: InkWell(
                  onTap: (){
                    Router.navigator.pushNamed(Routes.contactUsScreen);
                  },
                  child: Text(
                    'Contact Us',
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: InkWell(
                  onTap: (){},
                  child: Text(
                    'Share App',
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: InkWell(
                  onTap: (){
                    Router.navigator.pushReplacementNamed(Routes.loginScreen);
                  },
                  child: Text(
                    'Logout',
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

// ignore: must_be_immutable
class ListTileOfDrawer extends StatelessWidget {
  IconData leading;
  String itemOfDrawerName;
  Function onPressed;

  ListTileOfDrawer({this.leading, this.itemOfDrawerName,this.onPressed});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(leading),
      title: Text(itemOfDrawerName),
      trailing: Icon(Icons.arrow_forward_ios),
      onTap: onPressed,
    );
  }
}
