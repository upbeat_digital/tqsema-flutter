import 'package:flutter/material.dart';
import '../../../../domain/route/router.gr.dart';
import '../../../widgets/login/circle_button_with_border.dart';
import '../../../widgets/login/row_of_login.dart';

class ChooseWayToResetPassScreen extends StatelessWidget {
  ChooseWayToResetPassScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            color: Colors.green,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
          ),
          SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: SafeArea(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  RowOfTheTopLogin(
                    headerName: 'Reast Password',
                    color: Colors.white,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 70.0),
                    child: ImageIcon(
                      AssetImage("images/recover_icon.png"),
                      size: 150,
                      color: Colors.white,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: Text(
                      'Choose Your Recovery Method?',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      ),
                    ),
                  ),
                  CircleButtonWithBorder(
                    onPress: () {
                      Router.navigator.pushNamed(Routes.recoverViaEmail);
                    },
                    buttonName: 'Recover via email address',
                    iconData: Icons.mail,
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  CircleButtonWithBorder(
                    buttonName: 'Recover via mobile number',
                    onPress: (){
                      Router.navigator.pushNamed(Routes.recoverViaPhoneScreen);

                    },
                    iconData: Icons.phone,
                  ),
                  SizedBox(
                    height: 30,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
