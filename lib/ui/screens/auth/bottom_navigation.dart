import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tqsema_final/ui/screens/nav_screens/fields/fields.dart';
import 'package:tqsema_final/ui/screens/nav_screens/games/games.dart';
import 'package:tqsema_final/ui/screens/nav_screens/home/home.dart';
import 'package:tqsema_final/ui/screens/nav_screens/messages/messages.dart';



class BottomNavigationWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return BottomNavigationWidgetState();
  }
}

class BottomNavigationWidgetState extends State<BottomNavigationWidget> {
  int _selectedTab = 0;
  final _pageOptions = [
    HomeScreen(),
    FieldsScreen(),
    GamesScreen(),
    MessagesScreen(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pageOptions[_selectedTab],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedTab,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Colors.green,
        unselectedItemColor: Colors.grey,
        elevation: 0,
        
        onTap: (int index) {
          setState(() {
            _selectedTab = index;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: ImageIcon(
              AssetImage("images/icon_home.png"),
            ),
            title: Text('home'),
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(
              AssetImage("images/fields_icon.png"),
            ),
            title: Text('Fields'),
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(
              AssetImage("images/game_icon.png"),
            ),
            title: Text('Games'),
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(
              AssetImage("images/icon_message.png"),
            ),
            title: Text('Messages'),
          ),
        ],
      ),
    );
  }
}
