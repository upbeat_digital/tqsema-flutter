import 'package:flutter/material.dart';
import '../../../widgets/login/green_button.dart';
import '../../../widgets/login/txt_field_of_login.dart';



class RecoverViaPhoneScreen extends StatelessWidget {
  RecoverViaPhoneScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: IconButton(
                  onPressed: (){},
                  icon: Icon(Icons.arrow_back_ios),
                ),
              ),
              Center(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 12.0),
                      child: Text(
                        'Recover via mobile number',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 24,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 52.0,
                        vertical: 16,
                      ),
                      child: Text(
                        'Enter your phone number below and get\n      sms pin to activate your account',
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.black54,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.visible,
                      ),
                    ),
                     Center(
                    child: Form(
                      autovalidate: true,
                     // key: _formKey,
                      child: TextFieldOfLogin(
                        image: Image.asset('images/verify/emirates_logo.png'),
                      //  controller: _phoneNumberController,
//                        inputType: TextInputType.phone,
                        hint: ' xx xxx xxx',
                      ),
                    ),
                  ),
                
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 50, left: 16.0, right: 16 , bottom: 10),
                      child: GreenButton(
                        buttonName: 'Next',
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
