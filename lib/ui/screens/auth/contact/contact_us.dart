import 'package:flutter/material.dart';
import '../../../widgets/login/the_green_part_of_login.dart';
import '../../../widgets/login/txt_field_of_login.dart';

class ContactUsScreen extends StatelessWidget {
  ContactUsScreen({Key key}) : super(key: key);

  ///completed ui
  ///m7tgen n3rf httb3t 3la feen

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 650,
          child: Column(
            children: <Widget>[
              TheGreenPartOfTheLogin(
                myIcon: Icons.message,
                title: 'Contact us',
              ),
              Expanded(
                flex: 2,
                child: Stack(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(18.0),
                          child: Card(
                            child: SizedBox(
                              width: MediaQuery.of(context).size.width * .9,
                              height: 320,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 50,
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: TextFieldOfLogin(
                            hint: 'Name',
                            context: context,
                          ),
                        ),
                        TextFieldOfLogin(
                          hint: 'Email',
                          context: context,
                        ),
                        TextFieldOfLogin(
                          hint: 'Message',
                          context: context,
                        ),
                        SizedBox(
                          height: 50,
                        ),
                        SizedBox(
                          child: CircleAvatar(
                            radius: 40,
                            backgroundColor: Colors.green,
                            child: Icon(
                              Icons.send,
                              size: 40,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
//SizedBox(
//                  child:
//                  CircleAvatar(
//                    radius: 40,
//                    backgroundColor: Colors.green,
//                    child: Icon(Icons.send,size: 40,color: Colors.white,),
//                  ),
////                    IconButton(
////                      color: Colors.green,
////                      icon: Icon(Icons.send),
////                      padding: EdgeInsets.all(8),
////                    ),
//                ),
