import 'package:flutter/material.dart';
import 'package:tqsema_final/domain/auth/auth_service.dart';
import 'package:tqsema_final/locator.dart';

class VMOfRegister extends ChangeNotifier {
  bool success = false;
  final AuthenticationService auth = locator<AuthenticationService>();

  void pushPage(BuildContext context, Widget page) {
    Navigator.of(context).push(
      MaterialPageRoute<void>(builder: (_) => page),
    );
  }

  void logInWithEmailAndPassword({
    String email,
    String password,
  }) async {
    //success = await auth.signUpWithEmail(email: email, password: password);
  }
}
