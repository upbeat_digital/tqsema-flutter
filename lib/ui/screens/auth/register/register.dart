import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';
import 'package:tqsema_final/domain/auth/auth_service.dart';
import 'package:tqsema_final/domain/route/router.gr.dart';
import '../../../widgets/login/green_button.dart';
import '../../../widgets/login/the_green_part_of_login.dart';
import '../../../widgets/login/txt_field_of_login.dart';
import 'vm_register.dart';

///completed UI and responsive UI

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  VMOfRegister vmOfRegister = VMOfRegister();
  final TextEditingController _emailEditController = TextEditingController();
  final TextEditingController _passEditController = TextEditingController();
  final TextEditingController _nameEditController = TextEditingController();
  final TextEditingController _positionEditController = TextEditingController();
  final TextEditingController _cityEditController = TextEditingController();
  final TextEditingController _dateEditController = TextEditingController();
  bool _isloading = false;

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<AuthenticationService>(context, listen: false);

    //ScreenUtil.init(context);

    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
         // height: ScreenUtil.mediaQueryData.size.height * 1.370,
          child: Column(
            children: <Widget>[
              TheGreenPartOfTheLogin(
                title: tr('messages.register'),
                myIcon: Icons.person,
              ),
              Expanded(
                flex: 3,
                child: Column(
                  children: <Widget>[
                    TextFieldOfLogin(
                      controller: _nameEditController,
                      context: context,
                      hint: tr('messages.name'),
                    ),
                    TextFieldOfLogin(
                      controller: _emailEditController,
                      context: context,
                      hint: tr('messages.email'),
                    ),
                    // PassWordTextField(
                    //   passEditController: _passEditController,
                    //   passwordVisible: false,
                    // ),
                    // FormBuilder(
                    //   child: Padding(
                    //     padding: const EdgeInsets.symmetric(
                    //         horizontal: 50, vertical: 18),
                    //     child: FormBuilderDateTimePicker(
                    //       attribute: "date picker",
                    //       controller: _dateEditController,
                    //       initialDatePickerMode: DatePickerMode.year,
                          
                    //   //    initialValue: DateTime.parse('Enter date'),
                    //       textAlign: TextAlign.center,
                    //       inputType: InputType.date,
                    //       format: DateFormat.yMd(),
                    //      // initialDate: null,
                    //     ),
                    //   ),
                    // ),
            

                    TextFieldOfLogin(
                        controller: _dateEditController,
                        context: context,
                        hint: tr('messages.date_of_birth')),
                    TextFieldOfLogin(
                      controller: _positionEditController,
                      context: context,
                      hint: tr('messages.postion'),
                    ),
                    TextFieldOfLogin(
                      controller: _cityEditController,
                      context: context,
                      hint: tr('messages.city'),
                    ),
                    _isloading
                        ? CircularProgressIndicator()
                        : Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: GreenButton(
                              buttonName: tr('messages.register'),
                              function: () {
                                // setState(() {
                                //   _isloading = true;
                                // });
                                // provider
                                //     .signUp(
                                //   _emailEditController.text,
                                //   _passEditController.text,
                                //   _nameEditController.text,
                                //   _dateEditController.text,
                                //   _positionEditController.text,
                                //   _cityEditController.text,
                                //   context,
                                // )
                                //     .then((onValue) {
                                //   Router.navigator
                                //       .pushNamed(Routes.loginScreen);
                                // });
                                // Future.delayed(Duration(seconds: 1), () {
                                //   setState(() {
                                //     _isloading = false;
                                //   });
                                //   print('is loading is : $_isloading');
                                // });
                              },
//                        fun: vmOfRegister.logInWithEmailAndPassword(),
                            ),
                          ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _dateEditController.dispose();
    _cityEditController.dispose();
    _positionEditController.dispose();
    _emailEditController.dispose();
    _nameEditController.dispose();
    _passEditController.dispose();
    super.dispose();
  }
}
