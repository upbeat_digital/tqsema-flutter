import 'package:flutter/material.dart';
import 'package:tqsema_final/domain/auth/auth_service.dart';
import 'package:tqsema_final/locator.dart';

class VMOfVerify
//    extends ChangeNotifier
{
  bool success = false;
  final AuthenticationService auth = locator<AuthenticationService>();

  void pushPage(BuildContext context, Widget page) {
    Navigator.of(context).push(
      MaterialPageRoute<void>(builder: (_) => page),
    );
  }

  verify(TextEditingController _phoneNumberController) {
    auth.verifyPhoneNumber(_phoneNumberController);
  }
//  }
}
