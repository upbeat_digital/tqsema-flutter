import 'package:flutter/material.dart';
//import 'package:flutter/widgets.dart';

import 'verify.dart';
import 'verify_sliver_child.dart';

///first screen in the app
///add phone number
///to get sms messages
///and from this screen to add the code in the next screen

class SliverVerifyScreen extends StatefulWidget {
  const SliverVerifyScreen({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SliverVerifyScreenState();
}

class _SliverVerifyScreenState extends State<SliverVerifyScreen> {
////  bool _pinned = true;
//  bool _snap = false;
//  bool _floating = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        elevation: 0,
        title:Text(
          "TQSEMA",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 23,
          ),
        ),
        centerTitle: true,
        actions: <Widget>[
          Row(
            children: <Widget>[
              IconButton(
                  icon: Icon(Icons.language,size: 29,),
                  onPressed: (){}
                  ),
            ],
          ),
        ],
      ),
      backgroundColor: Color(0xff248332),
//      backgroundColor: Colors.transparent,
      // CustomScrollView.
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
//                shape: RoundedRectangleBorder(
//                  borderRadius: BorderRadius.all(
//                    Radius.circular(90),
//                  ),
//                ),
//                primary: true,
            automaticallyImplyLeading: false,
            stretchTriggerOffset: 1,
            backgroundColor: Colors.white,
            pinned: true,
            snap: false,
            floating: true,
            stretch: true,
            expandedHeight: 400.0,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,

              title: Padding(
                padding: const EdgeInsets.only(
                  top: 30.0,
                ),
//                child: Text(
//                  'TQSEMA',
//                  style: TextStyle(
//                    fontSize: 20,
//                    color: Colors.black,
//                  ),
//                ),
              ),
              background:

//                  Container(
//                    color: Colors.transparent,
//                  ),
              Image.asset(
                'images/verify/verify_small.png',
                fit: BoxFit.cover,
              ),
//                  Image.asset(
//                    'images/verify/verify.png',
//                    fit: BoxFit.cover,
//                  ),
            ),
          ),
          // If the main content is a list, use SliverList instead.
          SliverFillRemaining(
            ///lw 3aiz t3'er 3'er hna
            hasScrollBody: false,
            fillOverscroll: false,
            child: Container(
//              color: Colors.transparent,
                decoration: BoxDecoration(
//                  color: Colors.white,
                  color: Color(0xff248332),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40),
                  ),
                ),
              child: VerifySliverChild(),

            ),
          ),
        ],
      ),
//      bottomNavigationBar: this._getBottomVerify(),
    );
  }
}
