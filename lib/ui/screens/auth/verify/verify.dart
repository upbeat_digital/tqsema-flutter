import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:tqsema_final/ui/widgets/login/green_button.dart';
import 'package:tqsema_final/ui/widgets/login/txt_field_of_login.dart';

import 'vm_verify.dart';

//class Verify extends StatefulWidget {
//  @override
//  State<StatefulWidget> createState() {
//    return VerifyState();
//  }
//}
class VerifyScreen extends StatefulWidget {
  @override
  _VerifyScreenState createState() => _VerifyScreenState();
}

class _VerifyScreenState extends State<VerifyScreen> {
  final TextEditingController _phoneNumberController = TextEditingController();

//  final TextEditingController _smsController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  bool agreeTerms = false;

//  String _message = '';
//  String _verificationId;
  VMOfVerify vmOfVerify = VMOfVerify();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      backgroundColor: Colors.transparent,
      body: SafeArea(
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'TQSEMA',
                  style: TextStyle(fontSize: 30),
                ),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
//          padding: EdgeInsets.all(20.0),
//          shrinkWrap: true,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 30.0),
                      child: Text(
                        'Hey, what\'s your number ?',
                        style: TextStyle(fontSize: 22),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 45.0),
                      child: Text(
                        'Enter your phone number below and get \n\t\t\t\t\t\t\t sms pin to activate your account',
                        style: TextStyle(color: Colors.grey),
                      ),
                    ),
                    Center(
                      child: Form(
                        autovalidate: true,
                        key: _formKey,
                        child: TextFieldOfLogin(
                          image: Image.asset('images/verify/emirates_logo.png'),

                          controller: _phoneNumberController,
//                        inputType: TextInputType.phone,
                          hint: ' xx xxx xxx',
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Checkbox(
                            value: agreeTerms,
                            onChanged: (isChecked) {
                              setState(() {
                                agreeTerms = isChecked;
                              });
                            },
                          ),
                          RichText(
                            text: TextSpan(
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14.0,
                              ),
                              children: <TextSpan>[
                                TextSpan(text: 'I agree to the  '),
                                TextSpan(
                                  text:
//                            LocalizationUtils.getMessageText(
//                                context,
                                      'Terms_Condition',
//                            ),
                                  style: TextStyle(color: Colors.green),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () async {
//                                String url = Constants.PRIVACY_POLICY;
//                                if (await canLaunch(url)) {
//                                  await launch(url);
//                                }
                                    },
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 18.0),
                      child: GreenButton(
                        buttonName: 'Next',
                        buttonColor: Colors.green,
                        function: () {
                          vmOfVerify.verify(_phoneNumberController);

//                  if (_formKey.currentState.validate()) {
//                    if (!agreeTerms) {
//                      AppUtility().showMessageDialog(
//                          context,
//                          LocalizationUtils.getInputText(
//                              context, 'accept_terms_condition'),);
//                    } else {
//                      sendOtp('+${_country.phoneCode}', phoneController.text);
//                    }
//                  }
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _phoneNumberController.dispose();
    super.dispose();
  }
}
