import 'package:flutter/material.dart';
import '../../../widgets/login/green_button.dart';
import '../../../widgets/login/txt_field_of_login.dart';

class RecoverViaEmailScreen extends StatelessWidget {
  RecoverViaEmailScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              IconButton(
                onPressed: (){},
                icon: Icon(Icons.arrow_back_ios),
              ),
              Center(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 12.0),
                      child: Text(
                        'Recover via email address',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 24,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 52.0,
                        vertical: 16,
                      ),
                      child: Text(
                        'Type your email address below and\n    you will get a verification code',
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.black54,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.visible,
                      ),
                    ),
                    TextFieldOfLogin(
                      hint: 'Your email',
                      context: context,
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 50, left: 16.0, right: 16),
                      child: GreenButton(
                        buttonName: 'Next',

                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
