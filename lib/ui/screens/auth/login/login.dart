import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:provider/provider.dart';
import 'package:tqsema_final/domain/auth/auth_service.dart';
import 'package:tqsema_final/domain/route/router.gr.dart';
import '../../../widgets/login/green_button.dart';
import '../../../widgets/login/the_green_part_of_login.dart';
import '../../../widgets/login/txt_field_of_login.dart';

import 'vm_login.dart';

///completed UI and responsive UI

// ignore: must_be_immutable
class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

//////////// login not completed //////////////////
class _LoginScreenState extends State<LoginScreen> {
  VMOfLogin vmOfLogin = VMOfLogin();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool _isloading = false;
  bool _saving = false;

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<AuthenticationService>(context, listen: false);
   // ScreenUtil.init(context);
    return Scaffold(
      body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
           // height: ScreenUtil.mediaQueryData.size.height * 1.0300,
            child: Column(
              children: <Widget>[
                TheGreenPartOfTheLogin(
                  title: tr('messages.login'),
                  myIcon: Icons.lock,
                ),
                Expanded(
                  flex: 2,
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        TextFieldOfLogin(
                          context: context,
                          hint: tr('messages.email'),
                          controller: _emailController,
                        ),
                        // PassWordTextField(
                        //   passEditController: _passwordController,
                        //   passwordVisible: false,
                        // ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: GreenButton(
                            buttonName: tr('messages.login'),
                            function: () async {
                              if (!_formKey.currentState.validate()) {
                                return;
                              }
                              _formKey.currentState.save();
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                              setState(() {
                                _saving = true;
                              });

                              provider
                                  .signInWithEmailAndPassword(
                                      _emailController.text,
                                      _passwordController.text,
                                      context)
                                  .then(
                                (onValue) {
                                  Router.navigator.pushReplacementNamed(
                                      Routes.bottomNavigation);
                                },
                              );

                              Future.delayed(
                                Duration(seconds: 1),
                                () {
                                  setState(() {
                                    _saving = false;
                                  });
                                  print('is loading is : $_isloading');
                                },
                              );
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: GreenButton(
                            buttonName: tr('messages.register'),
                            function: () {
                              Router.navigator.pushNamed(Routes.registerScreen);
                            },
                          ),
                        ),
                        FlatButton(
                          onPressed: () {
                            Router.navigator.pushNamed(Routes.resetPassScreen);
                          },
                          child: Text(
                            tr('messages.forget-password'),
                            style: TextStyle(color: Colors.grey, fontSize: 16),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
       
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }
}
