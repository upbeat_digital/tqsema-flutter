import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../widgets/login/row_of_login.dart';

class ChoosePositions extends StatelessWidget {
  ChoosePositions({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color(0xfffafafa),
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              RowOfTheTopLogin(
                headerName: 'Postion',
                color: Colors.black,
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 17.0, horizontal: 8),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * .8,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 30,
                        ),
                        PostionItem(postionName: 'Center Attacking Midfielder'),
                        PostionItem(postionName: 'Center Back'),
                        PostionItem(postionName: 'Center Defending Midfielder'),
                        PostionItem(postionName: 'Goal Keeper'),
                        PostionItem(postionName: 'Left Back'),
                        PostionItem(postionName: 'Left Midfied'),
                        PostionItem(postionName: 'Right Wing'),
                        PostionItem(postionName: 'Stricker'),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class PostionItem extends StatelessWidget {
  String postionName = '';

  PostionItem({this.postionName});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            postionName,
            style: TextStyle(
              fontSize: 20,
              color: Colors.black,
            ),
          ),
        ),
        Divider(),
      ],
    );
  }
}
