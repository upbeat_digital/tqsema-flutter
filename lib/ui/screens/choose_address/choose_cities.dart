import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../widgets/login/row_of_login.dart';

class ChooseCities extends StatelessWidget {
  ChooseCities({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color(0xfffafafa),
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              RowOfTheTopLogin(
                headerName: 'Cities',
                color: Colors.black,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical:17.0,horizontal: 8),
                child: Container(
                 
                 width: MediaQuery.of(context).size.width,
                 height: MediaQuery.of(context).size.height* 0.8,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 20,
                        ),
                        CityItem(cityName: 'Dubai'),
                        CityItem(cityName: 'Abu Dahabi'),
                        CityItem(cityName: 'Sharjah'),
                        CityItem(cityName: 'Al Ain'),
                        CityItem(cityName: 'Ajman'),
                        CityItem(cityName: 'Ras Al Khaimah'),
                        CityItem(cityName: 'Fujairah'),
                      
                       
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class CityItem extends StatelessWidget {
  String cityName = '';

  CityItem({this.cityName});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            cityName,
            style: TextStyle(fontSize: 20),
          ),
        ),
        Divider(),
      ],
    );
  }
}
