import 'package:flutter/material.dart';
import '../../widgets/players/player_card.dart';

// ignore: must_be_immutable
class PlayersScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
//        leading: Icon(Icons.arrow_back_ios),
        title: Text('Players'),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 14.0),
        child: GridView(
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 250,
            childAspectRatio: (MediaQuery.of(context).textScaleFactor) / 0.90,
            mainAxisSpacing: 4,
            crossAxisSpacing: 10,
          ),
          children: <Widget>[
            PlayerCard(),
            PlayerCard(),
            PlayerCard(),
            PlayerCard(),
            PlayerCard(),
            PlayerCard(),
            PlayerCard(),
          ],
        ),
      ),
    );
  }
}
