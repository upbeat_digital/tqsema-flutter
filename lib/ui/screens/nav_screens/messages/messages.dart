import 'package:flutter/material.dart';
import 'package:tqsema_final/ui/screens/drawer.dart';
import 'package:tqsema_final/ui/widgets/dwidgets/shared/customeappbar.dart';

// ignore: must_be_immutable
class MessagesScreen extends StatelessWidget {
  String dummyImage = 'https://i.ytimg.com/vi/aZCLl_2nJDI/maxresdefault.jpg';
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _drawerKey,
        drawer: MyDrawer(),
        //it will be find in  UI / widgets/ dwidgets/shared
        appBar: CustomeAppBar(
          text: 'Messages',
        ),
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: ListView(
            children: <Widget>[
              ItemOfMessages(image: dummyImage),
              ItemOfMessages(image: dummyImage),
              ItemOfMessages(image: dummyImage),
            ],
          ),
        ));
  }
}

// ignore: must_be_immutable
class ItemOfMessages extends StatelessWidget {
  String image;

  ItemOfMessages({this.image});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
      elevation: 2,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      child: ListTile(
        leading: CircleAvatar(
          backgroundImage: NetworkImage(image),
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('Emirates Court'),
            SizedBox(
              width: 100,
            ),
            Icon(Icons.calendar_today,size: 16,),
            Text(
              '5 min ago',
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 12,
                  color: Colors.grey),
            ),
          ],
        ),
        subtitle: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'You have Joinedto the team of the game, you are the player number 5',
            style: TextStyle(
              fontSize: 12 / (MediaQuery.of(context).textScaleFactor),
            ),
          ),
        ),
      ),
    );
  }
}

//        backgroundColor: Colors.white,
//        unselectedItemColor: AppColors.appBlackColor,
//        currentIndex: barIndex = 0,
//        type: BottomNavigationBarType.fixed,
//
