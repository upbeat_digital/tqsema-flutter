import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tqsema_final/ui/screens/drawer.dart';
import 'package:tqsema_final/ui/widgets/dwidgets/shared/customeappbar.dart';
import '../../../widgets/item_of_games.dart';

// ignore: must_be_immutable
class GamesScreen extends StatelessWidget {
  String dummyImage = 'https://i.ytimg.com/vi/aZCLl_2nJDI/maxresdefault.jpg';
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _drawerKey,
        drawer: MyDrawer(),
         //it will be find in  UI / widgets/ dwidgets/shared
        appBar: CustomeAppBar(
          text: 'Games',
        ),
      body:SizedBox(
      //commit to dev
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: ListView(
//        shrinkWrap: false,

//        itemExtent: 300,
        children: <Widget>[
          ItemOfGame(image: dummyImage),
          ItemOfGame(image: dummyImage),
        ],
      ),
    )
    );
  }
}

//        backgroundColor: Colors.white,
//        unselectedItemColor: AppColors.appBlackColor,
//        currentIndex: barIndex = 0,
//        type: BottomNavigationBarType.fixed,
//
