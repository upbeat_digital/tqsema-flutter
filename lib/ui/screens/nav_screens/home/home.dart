import 'package:flutter/material.dart';
import 'package:tqsema_final/domain/route/router.gr.dart';
import 'package:tqsema_final/ui/screens/drawer.dart';
import '../../../widgets/column_of_2_home_lists.dart';
import '../../../widgets/image_alot_at_main.dart';
import '../../../widgets/other_players.dart';
import '../../../widgets/row_header_line.dart';

// ignore: must_be_immutable
class HomeScreen extends StatelessWidget {
  String dummyImage = 'https://i.ytimg.com/vi/aZCLl_2nJDI/maxresdefault.jpg';
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _drawerKey,
      drawer: MyDrawer(),
      body: ListView(
        children: <Widget>[
          Stack(
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      flex: MediaQuery.of(context).size.height >
                              MediaQuery.of(context).size.width
                          ? 2
                          : 7,
                      child: Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image:
                                AssetImage("images/home/home_background.png"),
                            fit: BoxFit.cover,
                          ),
                        ),
                        child: Column(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.topLeft,
                              child: IconButton(
                                color: Colors.white,
                                icon: Icon(Icons.menu),
                                onPressed: () {
                                  _drawerKey.currentState.openDrawer();
                                },
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 10,
                                bottom: 5,
                              ),
                              child: Container(
                                height: 100,
                                width: double.infinity,
                                child: ListView(
                                  scrollDirection: Axis.horizontal,
                                  children: <Widget>[
                                    SizedBox(
                                      width: 20,
                                    ),
                                    NavScreenImage(dummyImage,
                                        width: 200, height: 10),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    NavScreenImage(dummyImage,
                                        width: 200, height: 10),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      //// for upcoming games section///////***
                      flex: 5,
                      child: SingleChildScrollView(
                        child: Column(
                          children: <Widget>[
                            HomeHeaderRow(
                              small: false,
                              function: () {},
                              bold: 'Upcoming Games',
                              green: 'Viaw all',
                            ),
                            SizedBox(
                              height: 140,
                              width: double.infinity,
                              child: ListView(
                                scrollDirection: Axis.horizontal,
                                children: <Widget>[
                                  SizedBox(
                                    width: 10,
                                  ),
                                  ColumnOf2HomeLists(
                                    b: true,
                                    dummyImage: dummyImage,
                                  ),
                                  ColumnOf2HomeLists(
                                    b: true,
                                    dummyImage: dummyImage,
                                  ),
                                  ColumnOf2HomeLists(
                                    b: true,
                                    dummyImage: dummyImage,
                                  ),
                                ],
                              ),
                            ),
                            Card(
                              child: Column(
                                children: <Widget>[
                                  //popular fields section
                                  HomeHeaderRow(
                                    function: () {

                                    },
                                    bold: 'Popular Fields',
                                    green: 'Viaw all',
                                  ),
                                  SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        SizedBox(
                                          width: 10,
                                        ),
                                        ColumnOf2HomeLists(
                                          b: false,
                                          dummyImage: dummyImage,
                                        ),
                                        ColumnOf2HomeLists(
                                          b: false,
                                          dummyImage: dummyImage,
                                        ),
                                        ColumnOf2HomeLists(
                                          b: false,
                                          dummyImage: dummyImage,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),

                            ///// players section///***
                            HomeHeaderRow(
                              //// home header need to add function
                              bold: 'Players',
                              green: 'Viaw all',
                              function: (){
                                Router.navigator.pushNamed(Routes.playersScreen);
                              },
                            ),

                            //// the players
                            SizedBox(
                              height: 65,
                              child: ListView(
                                scrollDirection: Axis.horizontal,
                                children: <Widget>[
                                  OtherPlayers(
                                    dummyImage: dummyImage,
                                    openPlayerInfo: () {},
                                  ),
                                  SizedBox(
                                    width: 15,
                                  ),
                                  OtherPlayers(
                                    dummyImage: dummyImage,
                                    openPlayerInfo: () {},
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              /// **** for drawer **
              /// de mlhash lzma
//          SafeArea(
//            child: IconButton(
//              icon: Icon(
//                Icons.menu,
//                color: Colors.white,
//              ),
//              onPressed: () =>
//                  HoldingScreen.scaffoldKey.currentState.openDrawer(),
//            ),
//          ),
            ],
          ),
        ],
      ),
    );
  }
}

//        backgroundColor: Colors.white,
//        unselectedItemColor: AppColors.appBlackColor,
//        currentIndex: barIndex = 0,
//        type: BottomNavigationBarType.fixed,
//
