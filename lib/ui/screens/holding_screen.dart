// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import 'holding_screen_provider.dart';
// import 'drawer.dart';
// import '../widgets/nav_bottom_bar.dart';

// // ignore: must_be_immutable
// class HoldingScreen extends StatelessWidget {
//   static GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

//   ProviderOfHoldingScreen providerOfHold;
//   List<String> navigationTitles = [
//     'Home',
//     'Fields',
//     'Games',
//     'Messages',
//   ];

//   @override
//   Widget build(BuildContext context) {
//     providerOfHold = Provider.of<ProviderOfHoldingScreen>(context);
//     return Scaffold(
//       key: ProviderOfHoldingScreen.getIndex() > 0 ? null : scaffoldKey,
//       drawer: MyDrawer(),
//       appBar: ProviderOfHoldingScreen.getIndex() > 0
//           ? AppBar(
//               title: Text(navigationTitles[ProviderOfHoldingScreen.getIndex()]),
// //      backgroundColor: Colors.green,
//             )
//           : null,
//       body:
//           providerOfHold.navigationClasses[ProviderOfHoldingScreen.getIndex()],
//       bottomNavigationBar:
//           MyBottomNavigationBar(providerOfHold: providerOfHold),
//     );
//   }
// }

// // ignore: must_be_immutable
// //class MyAppBar extends AppBar {
// //  List<String> navigationTitles = [
// //    'Home',
// //    'Games',
// //    'Fields',
// //    'Messages',
// //  ];
// //
// //  @override
// //  Widget build(BuildContext context) {
// //    return AppBar(
// ////      title: Text(navigationTitles[ProviderOfHoldingScreen.getIndex()],style: TextStyle(color: Colors.white),),
// //      title:Text(navigationTitles[ProviderOfHoldingScreen.getIndex()]) ,
// ////      backgroundColor: Colors.green,
// //    );
// //  }
// //}
// /*
// class MyHomePage extends StatefulWidget {
//   static String myHomePage = 'MyHomePage';
//   MyHomePage();
//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage>
//     with SingleTickerProviderStateMixin {
//   ProviderOfHome providerOfHome;
//   final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

//   static const TextStyle optionStyle =
//       TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
//   TabController _tabController;

//   @override
//   void initState() {
//     super.initState();
//     _tabController =
//         TabController(vsync: this, length: 5);
//     _tabController.animateTo(ProviderOfHome.getIndex(),curve: Curves.linear,duration: Duration(microseconds: 0));
//   }


//   @override
//   Widget build(BuildContext context) {
//     providerOfHome = Provider.of<ProviderOfHome>(context);
//     providerOfHome.context=context;
//     providerOfHome.setTabController(_tabController);
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         key: _scaffoldKey,
//         drawer: myDrawer(_scaffoldKey, context, providerOfHome),
//         body: Stack(
//           children: <Widget>[
//             flowerWidget(),
//             Column(
//               crossAxisAlignment: CrossAxisAlignment.stretch,
//               children: <Widget>[
//                 SizedBox(
//                   height: MediaQuery.of(context).size.height * .23,
//                   width: MediaQuery.of(context).size.width,
//                   child: Stack(
//                     children: <Widget>[
//                       Image.asset(
//                         'assets/clip_bg.png',
//                         fit: BoxFit.fill,
//                         width: MediaQuery.of(context).size.width,
//                         height: MediaQuery.of(context).size.height * .23,
//                       ),
//                       SafeArea(
//                         child: Padding(
//                           padding: EdgeInsets.symmetric(horizontal: 30),
//                           child: Row(
//                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                             crossAxisAlignment: CrossAxisAlignment.center,
//                             children: <Widget>[
//                               floatingButton((){_scaffoldKey.currentState.openDrawer();},icon: Icon(Icons.dehaze,color: Colors.black,)),
// //                              IconButton(
// //                                  icon: new Icon(Icons.dehaze),
// //                                  onPressed: () =>
// //                                      _scaffoldKey.currentState.openDrawer()
// //                              ),
//                               Padding(
//                                 padding: EdgeInsets.only(
//                                   top: 10,
//                                 ),
//                                 child: Image.asset(
//                                   'assets/logo.png',
//                                 ),
//                               ),
//                               floatingButton(providerOfHome.notifyLang,text: Language.myLanguage().myLanguage),

//                             ],
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//                 Flexible(
//                   flex: 10,
//                   child: TabBarView(
//                     dragStartBehavior: DragStartBehavior.start,
//                     physics: NeverScrollableScrollPhysics(),
//                     controller: _tabController,
//                     children: providerOfHome.navigationClasses,
//                   ),
//                 ),
//               ],
//             ),
//           ],
//         ),
//         bottomNavigationBar: myBottomNavigationBar(
//             providerOfHome: providerOfHome, controller: _tabController),
//       ),
//     );
//   }

//   @override
//   void dispose() {
//     _tabController.dispose();
//     super.dispose();
//   }

//   Widget flowerWidget() {
//     return Container(
//       decoration: new BoxDecoration(
//         color: Theme.of(context).backgroundColor,
//         image: new DecorationImage(
//             image: AssetImage('assets/pattern.png'), fit: BoxFit.cover),
//       ),
//       height: MediaQuery.of(context).size.height,
//       width: MediaQuery.of(context).size.width,
//       child: new BackdropFilter(
//         filter: new ImageFilter.blur(sigmaX: .5, sigmaY: .5),
//         child: new Container(
//           decoration: new BoxDecoration(color: Colors.white.withOpacity(0.0)),
//         ),
//       ),
//     );
//   }
// }


// * */
